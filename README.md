Controle de usuários utilizando tecnologias web.
Utilizamos o Electron framework para criar uma aplicação desktop utilizando web !!! TOP !

## Requisitos ##

* NPM
- Angular
- Electron
- Asp.NET WebApi
- Grunt


Para rodar o projeto siga os passos:

## 1 - Instalar o Electron em modo global ##

~~~
npm install -g electron-prebuilt
~~~

## 2 - Instalar as linhas de comando do Grunt global

~~~
npm install grunt-cli -g
~~~

## 3 - Clonar o repositório

Usando o menu do próprio BitBucket clique em Clone e execute o comando gerado em seu cmd em alguma pasta que deve queira salvar ;)

Após o clone feito, navege para a pasta do projeto

~~~
cd controle-alunos/app/
~~~

## 4 - Abrir projeto api-net no Visual Studio para ele configurar no IIS o serviço

após aberto VS e incluir sua aplicação no IIS vc poderá acessar a url via: `http://localhost/controle-alunos`

o banco de dados será criado automáticamente com o nome `BancoDemo`

## 5 - Setup aplicação.

### Dependências
somente na primeira vez: `npm install`

### Compilar arquivos js/css
após ter instalado as dependências: `grunt dev`

### Rodar aplicação
em um outro terminal digite `npm start`
