﻿using ControleAlunosAPI.Core.Context;
using ControleAlunosAPI.Core.Entities;
using ControleAlunosAPI.Core.Repository;

namespace ControleAlunoAPI.Repository.Repositories
{
    public class RCouse : RepositoryBase<Course>, IRCourse
    {
        public RCouse(IDbBaseContext context)
            : base(context)
        {

        }
    }
}
