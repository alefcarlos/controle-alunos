﻿using ControleAlunos.Shared;
using ControleAlunosAPI.Core.Context;
using ControleAlunosAPI.Core.Entities;
using ControleAlunosAPI.Core.Exceptions.Repository;
using ControleAlunosAPI.Core.Repository;
using ControleAlunosAPI.Core.VM;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ControleAlunoAPI.Repository.Repositories
{
    public class RStudent : RepositoryBase<Student>, IRStudent
    {
        private readonly IRCourse _rCourse;
        public RStudent(IDbBaseContext context, IRCourse rCourse)
            : base(context)
        {
            _rCourse = rCourse;
        }

        public List<Course> GetAvailableCourses(int studentId)
        {
            var student = Get(x => x.StudentId == studentId).Include(x => x.Courses).FirstOrDefault();

            if (student == null)
                throw new RecordNotFoundException("Aluno não encontrado.");

            var coursesId = student.Courses.Select(x => x.CourseId);

            var courses = _rCourse
                            .List()
                            .Where(c => !coursesId.Any(s => s == c.CourseId))
                            .ToList();

            return courses;
        }

        public void AddCourse(int studentId, CourseToAddVM course)
        {
            //Obter o aluno
            var student = Get(x => x.StudentId == studentId).Include(x => x.Courses).FirstOrDefault();
            if (student == null)
                throw new RecordNotFoundException("Aluno não encontrado.");

            if (student.Courses.Any(c => c.CourseId == course.CourseId))
                throw new ExistsRecordException("Curso já está associado ao aluno.");

            //Obter o curso
            var _course = _rCourse.Get(course.CourseId);
            if (course == null)
                throw new RecordNotFoundException("Curso não encontrado.");

            var entity = new StudentCourse
            {
                StudentId = studentId,
                CourseId = course.CourseId,
                Start = course.Start
            };

            course.Days.ForEach(d => entity.Days.Add(new StudentCourseDay { Day = d }));

            student.Courses.Add(entity);

            Update(student);
        }

        public object GetCourses(int studentId)
        {
            var student = Get(x => x.StudentId == studentId)
                            .Include(x => x.Courses)
                            .Include("Courses.Student")
                            .Include("Courses.Course")
                            .Include("Courses.Days")
                            .FirstOrDefault();

            if (student == null)
                throw new RecordNotFoundException("Aluno não encontrado.");

            return student.Courses
                            .Select(c => new { c.CourseId, c.Course.Name, c.Status, c.Start, c.End, Days = c.Days.Select(d => d.Day).OrderBy(d => d).ToList() }).ToList();
        }

        public object GetValidCourses(int studentId)
        {
            var student = Get(x => x.StudentId == studentId)
                            .Include(x => x.Courses)
                            .Include("Courses.Student")
                            .Include("Courses.Course")
                            .Include("Courses.Days")
                            .FirstOrDefault();

            //Filtrar somente os válidos
            student.Courses = student.Courses.Where(c => c.Status).ToList();

            if (student == null)
                throw new RecordNotFoundException("Aluno não encontrado.");

            return student
                    .Courses
                    .Where(c => c.Status)
                    .Select(c => new { c.CourseId, c.Course.Name, c.Status, Days = c.Days.Select(d => d.Day).OrderBy(d => d).ToList() }).ToList();
        }

        public void ChangeCourseStatus(int studentId, int courseId)
        {
            //Obter o aluno
            var student = Get(x => x.StudentId == studentId).Include(x => x.Courses).FirstOrDefault();
            if (student == null)
                throw new RecordNotFoundException("Aluno não encontrado.");

            if (!student.Courses.Any(c => c.CourseId == courseId))
                throw new RecordNotFoundException("Curso não encontrado.");

            var courseToCHange = student.Courses.First(x => x.CourseId == courseId);

            courseToCHange.Status = !courseToCHange.Status;
            courseToCHange.End = !courseToCHange.Status ? DateTime.UtcNow : (DateTime?)null; 

            Update(student);
        }

        public List<Payment> GetPayments(int studentId)
        {
            var student = Get(x => x.StudentId == studentId)
                .Include(x => x.Payments)
                .Include("Payments.Course")
                .FirstOrDefault();

            if (student == null)
                throw new RecordNotFoundException("Aluno não encontrado.");

            return student.Payments.ToList();
        }

        public MemoryStream Export()
        {
            var students = List()
                            .ToList()
                            .Select(x => new StudentExcelVM
                            {
                                Nome = x.Name,
                                Idade = x.Age,
                                Sexo = x.Gender == EStudentGender.Female ? "Feminino" : "Masculino"
                            });


            return Functions.ExportToExcel(students);
        }

        public override void Delete(int id)
        {
            //deletar registros filhos
            var sql = @"DELETE FROM StudentCourseDay WHERE StudentId = @StudentId;
                        DELETE FROM StudentCourse WHERE StudentId = @StudentId
                        DELETE FROM Payment WHERE StudentId = @StudentId";

            db.dbContext.Database.ExecuteSqlCommand(sql, new SqlParameter("StudentId", id));

            base.Delete(id);
        }
    }
}
