﻿using System;
using System.IO;
using ControleAlunosAPI.Core.Context;
using ControleAlunosAPI.Core.Entities;
using ControleAlunosAPI.Core.Repository;
using ControleAlunos.Shared;
using ControleAlunosAPI.Core.VM;

namespace ControleAlunoAPI.Repository.Repositories
{
    public class RPayment : RepositoryBase<Payment>, IRPayment
    {
        public RPayment(IDbBaseContext context)
            : base(context)
        {

        }

        public MemoryStream ExportPivot()
        {
            //Obter todos os pagamentos
            var sql = @"
SELECT 
S.Name AS StudentName
,C.Name AS CourseName
,P.PaymentValue
, CASE P.[Month] 
	WHEN 1 THEN 'Jan'
	WHEN 2 THEN 'Fev'
	WHEN 3 THEN 'Mar'
	WHEN 4 THEN 'Abr'
	WHEN 5 THEN 'Mai'
	WHEN 6 THEN 'Jun'
	WHEN 7 THEN 'Jul'
	WHEN 8 THEN 'Ago'
	WHEN 9 THEN 'Set'
	WHEN 10 THEN 'Out'
	When 11 THEN 'Nov'
	WHEN 12 THEN 'Dez'
END AS [Month]
FROM Payment P 
INNER JOIN Student S ON S.StudentId = P.StudentId
INNER JOIN Course C on C.CourseId = P.CourseId
ORDER BY [Month]";

            var data = db.dbContext.Database.SqlQuery<PaymentExcelVM>(sql);

            var pivot = new AlefPivotTable<PaymentExcelVM>(data);

            pivot.AddRow(nameof(PaymentExcelVM.StudentName));
            pivot.AddRow(nameof(PaymentExcelVM.CourseName));

            pivot.AddColumn(nameof(PaymentExcelVM.Month));

            var value = pivot.AddData(nameof(PaymentExcelVM.PaymentValue));
            value.Format = "R$ #.##0,00;-R$ #.##0,00";

            return pivot.GetStream();
        }
    }
}
