﻿using ControleAlunosAPI.Core.Context;
using ControleAlunosAPI.Core.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ControleAlunoAPI.Data.Context
{
    public class ControleAlunoAPIContext : BaseContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public ControleAlunoAPIContext() : base("name=ControleAlunosAPIContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

#if DEBUG
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ControleAlunoAPIContext, ControleAlunosAPI.Data.Migrations.Configuration>());
#endif
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<StudentCourse> StudentCourses { get; set; }

    }
}
