﻿using System.Web.Http;
using System.Web.Http.Description;
using System;
using ControleAlunosAPI.Models;
using ControleAlunosAPI.Core.Repository;
using ControleAlunosAPI.Core.Entities;
using System.Net;
using ControleAlunosAPI.Core.Exceptions.Repository;
using System.Data.Entity;
using System.Collections.Generic;

namespace ControleAlunosAPI.APIControllers
{
    public partial class StudentController
    {
        // GET: api/students/5/available-courses
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{id:int}/courses/available")]
        public IHttpActionResult GetAvailableCourses(int id)
        {
            try
            {
                return ReturnSuccess(_rep.GetAvailableCourses(id));
            }
            catch (RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // GET: api/students/5/courses
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{id:int}/courses")]
        public IHttpActionResult GetCourses(int id)
        {
            try
            {
                return ReturnSuccess(_rep.GetCourses(id));
            }
            catch (RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // GET: api/students/5/courses/valid
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{id:int}/courses/valid")]
        public IHttpActionResult GetValidCourses(int id)
        {
            try
            {
                return ReturnSuccess(_rep.GetValidCourses(id));
            }
            catch (RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // POST: api/students/5/courses/
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{studentId:int}/courses")]
        public IHttpActionResult PostCourses(int studentId, CourseToAddVM course)
        {
            try
            {
                if (course.Days.Count == 0)
                    throw new Exception("Selecione pelo menos um dia para o curso.");

                _rep.AddCourse(studentId, course);

                return ReturnSuccess(_rep.GetCourses(studentId));
            }
            catch (RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (ExistsRecordException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // PUT: api/students/5/courses/2
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{studentId:int}/courses/{courseId:int}")]
        public IHttpActionResult PutCourses(int studentId, int courseId)
        {
            try
            {
                _rep.ChangeCourseStatus(studentId, courseId);

                return ReturnSuccess(_rep.GetCourses(studentId));
            }
            catch (RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (ExistsRecordException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }
    }
}