﻿using ControleAlunosAPI.Core.Entities;
using ControleAlunosAPI.Core.Exceptions.Repository;
using ControleAlunosAPI.Models;
using System;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace ControleAlunosAPI.APIControllers
{
    public partial class StudentController
    {
        // GET: api/students/5/payments
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{id:int}/payments")]
        public IHttpActionResult GetPayments(int id)
        {
            try
            {
                return ReturnSuccess(_rep.GetPayments(id));
            }
            catch (RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // POST: api/students/5/courses/2/payment
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{studentId:int}/courses/{courseId:int}/payment")]
        public IHttpActionResult PostCoursePayment(int studentId, int courseId, Payment entity)
        {
            try
            {
                if (entity.PaymentValue <= 0)
                    throw new Exception("Valor do pagamento deve ser maior que zero!");

                _paymentRep.Add(entity);
                
                return ReturnSuccess();
            }
            catch (RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (ExistsRecordException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }
    }
}
