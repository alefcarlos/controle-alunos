﻿using System.Web.Http;
using System.Web.Http.Description;
using System;
using ControleAlunosAPI.Models;
using ControleAlunosAPI.Core.Repository;
using ControleAlunosAPI.Core.Entities;
using System.Net;
using ControleAlunosAPI.Core.Exceptions.Repository;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace ControleAlunosAPI.APIControllers
{
    [RoutePrefix("api/students")]
    public partial class StudentController : BaseAPIController
    {
        private readonly IRStudent _rep;
        private readonly IRPayment _paymentRep;

        public StudentController(IRStudent rep, IRPayment paymentRep)
        {
            _rep = rep;
            _paymentRep = paymentRep;
        }

        // GET: api/students
        [Route("")]
        [ResponseType(typeof(APIDefaultResult))]
        public IHttpActionResult GetStudents()
        {
            try
            {
                var list = _rep.List();
                return ReturnSuccess(list);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // GET: api/Students/5/avaliable-courses
        [Route("{id:int}")]
        [ResponseType(typeof(APIDefaultResult))]
        public IHttpActionResult GetStudent(int id)
        {
            try
            {
                var student = _rep.Get(id);

                if (student == null)
                    return ReturnError("Aluno não encontrado.", HttpStatusCode.NotFound);

                return ReturnSuccess(student);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // POST: api/Students
        [ResponseType(typeof(APIDefaultResult))]
        [Route("")]
        public IHttpActionResult PostStudent(Student student)
        {
            try
            {
                _rep.Add(student);

                return ReturnSuccess(student);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // PUT: api/Students
        [ResponseType(typeof(APIDefaultResult))]
        [Route("")]
        public IHttpActionResult PutStudent(Student student)
        {
            try
            {
                _rep.Update(student);

                return ReturnSuccess(student);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        // DELETE: api/Students/5
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{id:int}")]
        public IHttpActionResult DeleteStudent(int id)
        {
            try
            {
                _rep.Delete(id);

                return ReturnSuccess("Estudante deletado com sucesso.");
            }
            catch(RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("export")]
        public HttpResponseMessage ExportPayments()
        {
            var _file = _rep.Export();

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(_file.ToArray())
            };
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "students.xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            _file.Close();
            return result;
        }

        /*
        // PUT: api/Students/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStudent(int id, Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != student.StudentId)
            {
                return BadRequest();
            }

            db.Entry(student).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        */
        private bool StudentExists(int id)
        {
            return _rep.Existis(id);
        }
    }
}