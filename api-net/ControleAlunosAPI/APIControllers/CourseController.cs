﻿using System.Web.Http;
using System.Web.Http.Description;
using System;
using ControleAlunosAPI.Models;
using ControleAlunosAPI.Core.Repository;
using ControleAlunosAPI.Core.Entities;
using System.Net;
using ControleAlunosAPI.Core.Exceptions.Repository;

namespace ControleAlunosAPI.APIControllers
{
    [RoutePrefix("api/courses")]
    public class CourseController : BaseAPIController
    {
        private IRCourse _rep;

        public CourseController(IRCourse rep)
        {
            _rep = rep;
        }

        // GET: api/courses
        [Route("")]
        [ResponseType(typeof(APIDefaultResult))]
        public IHttpActionResult GetCourses()
        {
            try
            {
                var list = _rep.List();
                return ReturnSuccess(list);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // GET: api/courses/5
        [Route("{id:int}")]
        [ResponseType(typeof(APIDefaultResult))]
        public IHttpActionResult GetCourse(int id)
        {
            try
            {
                var entity = _rep.Get(id);

                if (entity == null)
                    return ReturnError("curso não encontrado.", HttpStatusCode.NotFound);

                return ReturnSuccess(entity);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // POST: api/courses
        [ResponseType(typeof(APIDefaultResult))]
        [Route("")]
        public IHttpActionResult PostCourse(Course entity)
        {
            try
            {
                _rep.Add(entity);

                return ReturnSuccess(entity);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // DELETE: api/courses/5
        [ResponseType(typeof(APIDefaultResult))]
        [Route("{id:int}")]
        public IHttpActionResult DeleteCourse(int id)
        {
            try
            {
                _rep.Delete(id);

                return ReturnSuccess("Curso deletado com sucesso.");
            }
            catch(RecordNotFoundException ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        // POST: api/courses
        [ResponseType(typeof(APIDefaultResult))]
        [Route("")]
        public IHttpActionResult PutCourse(Course entity)
        {
            try
            {
                _rep.Update(entity);

                return ReturnSuccess(entity);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        /*
        // PUT: api/Students/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStudent(int id, Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != student.StudentId)
            {
                return BadRequest();
            }

            db.Entry(student).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        */
        private bool CourseExists(int id)
        {
            return _rep.Existis(id);
        }
    }
}