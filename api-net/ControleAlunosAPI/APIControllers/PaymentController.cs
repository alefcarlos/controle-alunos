﻿using System.Web.Http;
using System.Web.Http.Description;
using System;
using ControleAlunosAPI.Models;
using ControleAlunosAPI.Core.Repository;
using ControleAlunosAPI.Core.Entities;
using System.Net;
using ControleAlunosAPI.Core.Exceptions.Repository;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ControleAlunosAPI.APIControllers
{
    [RoutePrefix("api/payments")]
    public class PaymentController : BaseAPIController
    {
        private IRPayment _rep;

        public PaymentController(IRPayment rep)
        {
            _rep = rep;
        }

        [HttpGet]
        [Route("export")]
        public HttpResponseMessage ExportPayments()
        {
            var _file = _rep.ExportPivot();

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(_file.ToArray())
            };
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "payments.xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            _file.Close();
            return result;
        }

    }
}