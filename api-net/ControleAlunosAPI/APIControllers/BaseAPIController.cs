﻿using ControleAlunosAPI.Models;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;

namespace ControleAlunosAPI.APIControllers
{
    public class BaseAPIController : ApiController
    {
        protected IHttpActionResult ReturnSuccess()
        {
            return ReturnSuccess(null);
        }

        protected IHttpActionResult ReturnSuccess(object data)
        {
            return Ok(
                        new APIDefaultResult
                        {
                            Status = true,
                            Data = data,
                            Code = HttpStatusCode.Accepted
                        }
                );
        }

        protected IHttpActionResult ReturnError(string msg, HttpStatusCode code)
        {
            return Ok(
                        new APIDefaultResult
                        {
                            Status = false,
                            Message = msg,
                            Code = code
                        }
                );
        }
    }
}
