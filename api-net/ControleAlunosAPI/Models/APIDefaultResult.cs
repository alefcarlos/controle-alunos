﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ControleAlunosAPI.Models
{
    public class APIDefaultResult
    {
        public bool Status { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
        public HttpStatusCode Code { get; set; }
    }
}
