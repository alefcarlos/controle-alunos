﻿using System;
using System.Runtime.Serialization;

namespace ControleAlunosAPI.Core.Exceptions.Repository
{
    [Serializable]
    public class ExistsRecordException : Exception
    {
        public ExistsRecordException()
        {
        }

        public ExistsRecordException(string message) : base(message)
        {
        }

        public ExistsRecordException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExistsRecordException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}