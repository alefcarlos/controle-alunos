﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleAlunosAPI.Core.VM
{
    public class StudentExcelVM
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public string Sexo { get; set; }
    }
}
