﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleAlunosAPI.Core.VM
{
    /// <summary>
    /// Modelo para exportação de Excel.
    /// </summary>
    public class PaymentExcelVM
    {
        public string StudentName { get; set; }
        public string CourseName { get; set; }
        public decimal PaymentValue { get; set; }
        public string Month { get; set; }
    }
}
