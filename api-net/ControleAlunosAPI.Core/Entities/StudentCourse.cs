﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControleAlunosAPI.Core.Entities
{
    public class StudentCourse
    {
        public StudentCourse()
        {
            Status = true;
            Days = new HashSet<StudentCourseDay>();
        }

        [Key,
        Column(Order = 1)]
        public int StudentId { get; set; }
        public Student Student { get; set; }

        [Key,
        Column(Order = 2)]
        public int CourseId { get; set; }
        public Course Course { get; set; }

        public bool Status { get; set; }

        [Required]
        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public ICollection<StudentCourseDay> Days { get; set; }
    }

    public class CourseToAddVM
    {
        public List<DayOfWeek> Days { get; set; }
        public int CourseId { get; set; }
        public DateTime Start { get; set; }
    }
}
