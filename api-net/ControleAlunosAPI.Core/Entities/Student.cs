﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ControleAlunosAPI.Core.Entities
{
    public class Student : BaseModel
    {
        public Student()
        {
            Courses = new HashSet<StudentCourse>();
            Payments = new HashSet<Payment>();
        }

        [Key]
        public int StudentId { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        //public int BornMinutes { get; set; }

        [Required, MaxLength(11)]
        public string CPF { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(10)]
        public string PhoneNumber { get; set; }

        [Required, MaxLength(11)]
        public string CellphoneNumber { get; set; }

        [Required]
        public DateTime BornDate { get; set; }

        [Required, MaxLength(200)]
        public string Address { get; set; }

        public int Age => Math.Abs(BornDate.Subtract(DateTime.Today).Days / 365);

        public EStudentGender Gender { get; set; }

        public ICollection<StudentCourse> Courses { get; set; }

        public ICollection<Payment> Payments { get; set; }
    }

    public enum EStudentGender
    {
        Male,
        Female
    }
}
