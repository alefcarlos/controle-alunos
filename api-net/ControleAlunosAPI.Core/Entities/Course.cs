﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ControleAlunosAPI.Core.Entities
{
    public class Course : BaseModel
    {
        public Course()
        {
            Students = new HashSet<StudentCourse>();
        }

        [Key]
        public int CourseId { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public ICollection<StudentCourse> Students { get; set; }
    }
}
