﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ControleAlunosAPI.Core.Entities
{
    public class Payment : BaseModel
    {
        [Key]
        public int PaymentId { get; set; }

        public int StudentId { get; set; }
        public Student Student { get; set; }

        public int CourseId { get; set; }
        public Course Course { get; set; }

        public decimal PaymentValue { get; set; }

        public DateTime PayDay { get; set; } = DateTime.UtcNow;

        public int Month { get; set; }
    }
}
