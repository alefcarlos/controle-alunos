﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControleAlunosAPI.Core.Entities
{
    public class StudentCourseDay
    {
        [Key,
        Column(Order = 1)]
        public int StudentId { get; set; }
        public Student Student { get; set; }

        [Key,
        Column(Order = 2)]
        public int CourseId { get; set; }
        public Course Course { get; set; }

        [Key,
        Column(Order = 3)]
        public DayOfWeek Day { get; set; }
    }
}
