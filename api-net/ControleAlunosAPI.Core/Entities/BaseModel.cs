﻿using Newtonsoft.Json;
using System;

namespace ControleAlunosAPI.Core.Entities
{
    public class BaseModel
    {
        [JsonIgnore]
        public DateTime? _CreateDate { get; set; }

        [JsonIgnore]
        public DateTime? _UpdateDate { get; set; }
    }
}
