﻿using ControleAlunosAPI.Core.Entities;

namespace ControleAlunosAPI.Core.Repository
{
    public interface IRCourse : IRepositoryBase<Course>
    {
        
    }
}
