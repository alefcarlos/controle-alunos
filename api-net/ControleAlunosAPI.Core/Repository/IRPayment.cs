﻿using ControleAlunosAPI.Core.Entities;
using System.IO;

namespace ControleAlunosAPI.Core.Repository
{
    public interface IRPayment : IRepositoryBase<Payment>
    {
        MemoryStream ExportPivot();
    }
}
