﻿using ControleAlunosAPI.Core.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace ControleAlunosAPI.Core.Repository
{
    public interface IRStudent : IRepositoryBase<Student>
    {
        List<Course> GetAvailableCourses(int studentId);
        void AddCourse(int studentId, CourseToAddVM course);
        object GetCourses(int studentId);
        void ChangeCourseStatus(int studentId, int courseId);
        object GetValidCourses(int studentId);
        List<Payment> GetPayments(int studentId);
        MemoryStream Export();
    }
}
