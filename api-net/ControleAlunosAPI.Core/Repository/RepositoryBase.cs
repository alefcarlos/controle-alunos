﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq;
using System;
using System.Linq.Expressions;
using ControleAlunosAPI.Core.Context;
using ControleAlunosAPI.Core.Exceptions.Repository;
using ControleAlunosAPI.Core.Entities;

namespace ControleAlunosAPI.Core.Repository
{
    public interface IRepositoryBase<TModel>
        where TModel : BaseModel
    {
        IQueryable<TModel> List();

        IQueryable<TModel> Get(Expression<Func<TModel, bool>> predicate);
        TModel Get(params object[] id);

        void Add(TModel entity);
        void Update(TModel entity);
        void Delete(int id);
        void Delete(Func<TModel, bool> predicate);

        TModel First(Expression<Func<TModel, bool>> predicate);

        bool Existis(params object[] id);
    }

    public class RepositoryBase<TModel> : IDisposable, IRepositoryBase<TModel>
        where TModel : BaseModel
    {
        protected IDbBaseContext db;

        public RepositoryBase(IDbBaseContext db)
        {
            this.db = db;
        }

        public IQueryable<TModel> List()
        {
            return db.dbContext.Set<TModel>();
        }

        public IQueryable<TModel> Get(Expression<Func<TModel, bool>> predicate)
        {
            return db.dbContext.Set<TModel>().Where(predicate);
        }

        public TModel First(Expression<Func<TModel, bool>> predicate)
        {
            return db.dbContext.Set<TModel>().Where(predicate).FirstOrDefault();
        }

        public TModel Get(params object[] id)
        {
            var entity = db.dbContext.Set<TModel>().Find(id);

            return entity;
        }

        public virtual void Add(TModel entity)
        {
            entity._CreateDate = DateTime.UtcNow;

            db.dbContext.Set<TModel>().Add(entity);
            db.dbContext.SaveChanges();
        }
                
        public virtual void Delete(int id)
        {
            var entity = Get(id);
            if (entity == null)
                throw new RecordNotFoundException("Registro não existe.");

            db.dbContext.Set<TModel>().Remove(entity);
            db.dbContext.SaveChanges();
        }
               
        public virtual void Delete(Func<TModel, bool> predicate)
        {
            db.dbContext.Set<TModel>()
                .Where(predicate).ToList()
                .ForEach(del => db.dbContext.Set<TModel>().Remove(del));

            db.dbContext.SaveChanges();
        }
               
        public virtual void Update(TModel entity)
        {
            entity._UpdateDate = DateTime.UtcNow;
            db.dbContext.Entry(entity).State = EntityState.Modified;
            db.dbContext.SaveChanges();
        }
                
        public virtual bool Existis(params object[] id)
        {
            return db.dbContext.Set<TModel>().Find(id) != null;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.dbContext.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
