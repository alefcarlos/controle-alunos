﻿namespace ControleAlunosAPI.Core.Context
{
    public interface IDbBaseContext
    {
        BaseContext dbContext { get; }
    }
}