﻿using System;
using System.Data.Entity;

namespace ControleAlunosAPI.Core.Context
{
    public abstract class BaseContext : DbContext, IDbBaseContext
    {
        public BaseContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        public BaseContext dbContext => this;
    }
}
