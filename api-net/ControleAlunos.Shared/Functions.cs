﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleAlunos.Shared
{
    public static class Functions
    {
        /// <summary>
        /// Método para exportar uma lista para um arquivo excel. Os nomes das propriedades serão os nomes das colunas.
        /// </summary>
        /// <typeparam name="T">Tipo da lista.</typeparam>
        /// <param name="collection">Coleção para exportar.</param>
        /// <returns>Retorna um MemoryStram pronto para ser usado. NÃO ESQUECER DE CHAMAR O MÉTODO Close() para liberar a stream.</returns>
        public static MemoryStream ExportToExcel<T>(IEnumerable<T> collection)
        {
            ExcelPackage ePack;
            MemoryStream stream = new MemoryStream();

            //Criando o arquivo excel.
            using (ePack = new ExcelPackage(stream))
            {
                //Criando a planilha
                var dataSheet = ePack.Workbook.Worksheets.Add("Dados");
                dataSheet.Cells.LoadFromCollection(collection, true, OfficeOpenXml.Table.TableStyles.Light1);

                dataSheet.Cells.AutoFitColumns();
                ePack.Save();
            }


            stream.Position = 0;
            return stream;
        }
    }
}
