﻿using OfficeOpenXml;
using OfficeOpenXml.Table.PivotTable;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ControleAlunos.Shared
{
    /// <summary>
    /// Classe responsável para exportação de planilha dinâmica no padrão: Dados + Dinâmica.
    /// Usamos o EEPlus para gerar o arquivo excel. Podemos savá-lo em uma pasta ou obter o Stream do mesmo.
    /// Utilizamos o recurso LoadFromCollection para popular os Dados na primeira planilha.
    /// </summary>
    /// <typeparam name="T">Tipo dos dados.</typeparam>
    public class AlefPivotTable<T>
    {
        public const string DATA_SHEET_NAME = "Data";
        public const string PIVOT_SHEET_NAME = "Pivot";
        public const string PIVOT_START_CELL = "A3";

        /// <summary>
        /// Indica se a sheet de tabela dinâmica estará sempre na primeira posição. Por padrão é true.
        /// </summary>
        public bool PivotFirst { get; set; } = true;

        /// <summary>
        /// Indica se devemos exibir a planilha de dados, por padrão false.
        /// </summary>
        public bool DataSheetVisible { get; set; } = false;

        /// <summary>
        /// Coleção de dados.
        /// </summary>
        private readonly IEnumerable<T> DataSource;

        /// <summary>
        /// Referência para gerenciamento do arquivo.
        /// </summary>
        private ExcelPackage ExcelPackage;

        /// <summary>
        /// Lista de campos da planilha dinâmica.
        /// </summary>
        private List<AlefPivotTableField> Fields;

        /// <summary>
        /// Título do texto da área de dados da dinâmica.
        /// </summary>
        private string Caption = "";

        public AlefPivotTable(IEnumerable<T> source)
        {
            DataSource = source;
            Fields = new List<AlefPivotTableField>();
            Caption = "";
        }

        public AlefPivotTable(IEnumerable<T> source, string caption)
            : this(source)
        {
            Caption = caption;
        }

        /// <summary>
        /// Método para salvar o arquivo gerado em um local.
        /// </summary>
        /// <param name="fileName">Caminho do arquivo com extensão.</param>
        public void SaveAs(string fileName)
        {
            try
            {
                PrepareExcel();

                var pivot = PreparePivot();
                PrepareData(pivot);

                if (File.Exists(fileName))
                    File.Delete(fileName);

                ExcelPackage.SaveAs(new System.IO.FileInfo(fileName));
            }
            catch { throw; }
            finally
            {
                ExcelPackage.Dispose();
            }
        }

        /// <summary>
        /// Método para obter o Stream do arquivo. Dependendo da manipulação da mesma, não devemos esquecer de chamar o metodo Close().
        /// </summary>
        /// <returns>Obtem a stream do arquivo excel gerado na posição 0.</returns>
        public MemoryStream GetStream()
        {
            try
            {
                PrepareExcel();

                var pivot = PreparePivot();
                PrepareData(pivot);

                MemoryStream _stream = new MemoryStream();

                ExcelPackage.SaveAs(_stream);

                _stream.Position = 0;

                return _stream;
            }
            catch { throw; }
            finally
            {
                ExcelPackage.Dispose();
            }
        }


        /// <summary>
        /// Método para configurar a planilha dinâmica.
        /// </summary>
        /// <returns>Retorna a referência da PivotTable.</returns>
        private ExcelPivotTable PreparePivot()
        {
            //Obter a sheet de dados
            var dataSheet = ExcelPackage.Workbook.Worksheets[DATA_SHEET_NAME];

            //Validar se devemos exibir a sheet de dados
            dataSheet.Hidden = DataSheetVisible ? eWorkSheetHidden.Visible : eWorkSheetHidden.VeryHidden;

            //Obter todo o conteudo dos dados
            var range = dataSheet.Cells[dataSheet.Dimension.Address];

            var pivotSheet = ExcelPackage.Workbook.Worksheets.Add(PIVOT_SHEET_NAME);

            //Verificar se devemos exibir a pivot table primeiro.
            if (PivotFirst)
                ExcelPackage.Workbook.Worksheets.MoveToStart(PIVOT_SHEET_NAME);

            //Criando a planilha de pivot
            var pivotTable = pivotSheet.PivotTables.Add(pivotSheet.Cells[PIVOT_START_CELL], range, PIVOT_SHEET_NAME);

            pivotTable.MultipleFieldFilters = true;
            pivotTable.RowGrandTotals = true;
            pivotTable.ColumGrandTotals = true;
            pivotTable.Compact = true;
            pivotTable.CompactData = true;
            pivotTable.GridDropZones = false;
            pivotTable.Outline = false;
            pivotTable.OutlineData = false;
            pivotTable.ShowError = true;
            pivotTable.ErrorCaption = "[error]";
            pivotTable.ShowHeaders = true;
            pivotTable.UseAutoFormatting = true;
            pivotTable.ApplyWidthHeightFormats = true;
            pivotTable.ShowDrill = true;
            pivotTable.FirstDataCol = 3;
            pivotTable.DataOnRows = false;
            pivotTable.DataCaption = Caption;

            return pivotTable;
        }

        /// <summary>
        /// Método para adicionar uma campo de linha na dinâmica.
        /// </summary>
        /// <param name="source">Nome do campo.</param>
        /// <returns>Retorna referência do campo para configurações posteriores.</returns>
        public AlefPivotTableField AddRow(string source)
        {
            return AddField(source, EEinsteinPivotTableFieldType.Row);
        }

        /// <summary>
        /// Método para adicionar uma campo de filtro na dinâmica.
        /// </summary>
        /// <param name="source">Nome do campo.</param>
        /// <returns>Retorna referência do campo para configurações posteriores.</returns>
        public AlefPivotTableField AddFilter(string source)
        {
            return AddField(source, EEinsteinPivotTableFieldType.Filter);
        }

        /// <summary>
        /// Método para adicionar uma campo de coluna na dinâmica.
        /// </summary>
        /// <param name="source">Nome do campo.</param>
        /// <returns>Retorna referência do campo para configurações posteriores.</returns>
        public AlefPivotTableField AddColumn(string source)
        {
            return AddField(source, EEinsteinPivotTableFieldType.Col);
        }

        /// <summary>
        /// Método para adicionar uma campo de valor na dinâmica.
        /// </summary>
        /// <param name="source">Nome do campo.</param>
        /// <returns>Retorna referência do campo para configurações posteriores.</returns>
        public AlefPivotTableField AddData(string source)
        {
            return AddField(source, EEinsteinPivotTableFieldType.Data);
        }

        /// <summary>
        /// Método genérico para adição na lista de campos.
        /// </summary>
        /// <param name="source">Nome do campo.</param>
        /// <param name="type">Tipo do campo.</param>
        /// <returns></returns>
        private AlefPivotTableField AddField(string source, EEinsteinPivotTableFieldType type)
        {
            var field = new AlefPivotTableField { Source = source, Caption = source };

            switch (type)
            {
                case EEinsteinPivotTableFieldType.Row:
                    field.Orientation = EEinsteinPivotTableFieldType.Row;
                    Fields.Add(field);
                    break;
                case EEinsteinPivotTableFieldType.Filter:
                    field.Orientation = EEinsteinPivotTableFieldType.Filter;
                    Fields.Add(field);
                    break;
                case EEinsteinPivotTableFieldType.Col:
                    field.Orientation = EEinsteinPivotTableFieldType.Col;
                    Fields.Add(field);
                    break;
                case EEinsteinPivotTableFieldType.Data:
                    field.Orientation = EEinsteinPivotTableFieldType.Data;
                    Fields.Add(field);
                    break;
            }

            return field;
        }

        /// <summary>
        /// Método para adicionar os campos na tabela dinâmica.
        /// </summary>
        /// <param name="pivot">Referência da PivotTable.</param>
        private void PrepareData(ExcelPivotTable pivot)
        {
            ExcelPivotTableField field;
            Fields.OrderBy(x => x.Order).ToList().ForEach(x =>
            {
                field = pivot.Fields[x.Source];
                if (field == null)
                    throw new Exception($"Não existe o campo {x}");

                field.Sort = x.Sort;
                field.Name = x.Caption;

                switch (x.Orientation)
                {
                    case EEinsteinPivotTableFieldType.Row:
                        pivot.RowFields.Add(field);
                        break;
                    case EEinsteinPivotTableFieldType.Col:
                        pivot.ColumnFields.Add(field);
                        break;
                    case EEinsteinPivotTableFieldType.Data:
                        var fData = pivot.DataFields.Add(field);
                        fData.Name = $"Soma de {x.Caption}";
                        fData.Function = DataFieldFunctions.Sum;
                        fData.Format = x.Format;

                        break;
                    case EEinsteinPivotTableFieldType.Filter:
                        pivot.PageFields.Add(field);
                        break;
                }
            });
        }

        /// <summary>
        /// Método para configurar o arquivo excel e a planilha de dados.
        /// </summary>
        private void PrepareExcel()
        {
            ExcelPackage = new ExcelPackage();

            //Criando a planilha de dados
            var dataSheet = ExcelPackage.Workbook.Worksheets.Add(DATA_SHEET_NAME);
            dataSheet.Cells.LoadFromCollection(DataSource, true, OfficeOpenXml.Table.TableStyles.Light1);
        }
    }

    public class AlefPivotTableField
    {
        /// <summary>
        /// Origem do campo, nome da coluna na planilha de dados.
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Nome do campo para aparecer na tabela dinâmica.
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Indica qual é a orientação do campo, por padrão é Ascendente.
        /// </summary>
        public eSortType Sort { get; set; } = eSortType.Ascending;

        /// <summary>
        /// Indica se é filtro mútiplo, quando o campo for do tipo Filter.
        /// </summary>
        public bool MutipleFilter { get; set; }

        public EEinsteinPivotTableFieldType Orientation { get; set; }

        /// <summary>
        /// Indica qual é a ordem desse campo na hierarquia. Por padrão será na última posição.
        /// </summary>
        public int Order { get; set; } = int.MaxValue;

        public string Format { get; set; }
    }

    public enum EEinsteinPivotTableFieldType
    {
        Row,
        Col,
        Data,
        Filter
    }
}
