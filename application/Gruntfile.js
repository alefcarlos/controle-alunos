module.exports = function (grunt) {
  grunt.initConfig({
    jshint: {
      dev:  {
        src: ['app/assets/js/**/*js']
      }
    },
    concat: {
      dev: {
        src: [
              'app/assets/js/app/**/*js',
              'app/node_modules/checklist-model/checklist-model.js',
              'app/node_modules/angular-i18n/angular-locale_pt-br.js',
              'app/node_modules/moment/locale/pt-br.js',
              'app/node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
              'app/node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js'
            ],
        dest: 'app/assets/js/app-scripts.js'
      }
    },
    uglify: {
      dev: {
        src: ['app/assets/js/app-scripts.js'],
        dest: 'app/assets/js/app-scripts.min.js'
      }
    },
    cssmin: {
      dev: {
        src: [
              'app/node_modules/angular-bootstrap-datetimepicker/src/css/datetimepicker.css',
              'app/node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css',
              'app/node_modules/bootstrap-material-design/dist/css/ripples.min.css',
              'app/node_modules/snackbarjs/dist/snackbar.min.css',
              'app/node_modules/angularjs-datetime-picker/angularjs-datetime-picker.css',
              'app/assets/css/app.css'
            ],
          dest: 'app/assets/css/app-css.min.css'
      }
    },
    clean:{
      dev: [
              'app/assets/js/app-scripts.min.js',
              'app/assets/js/app-scripts.js'
          ],
      devCss: [
        'app/assets/css/app-css.min.css',
        'app/assets/css/app-style.min.css',
      ]
    },
    exec:{
      update: {
        command: 'npm install'
      },
      execute: {
        command: 'electron .'
      }
    },
    watch: {
      files: [
              'app/assets/js/app/**/*js',
              'app/assets/css/app.css'
            ],
      tasks: ['clean:dev', 'jshint:dev','concat:dev','clean:devCss', 'cssmin:dev'],
      options: {
        spawn: true,
        interrupt: false
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('dev', ['clean:dev', 'jshint:dev','concat:dev', 'clean:devCss', 'cssmin:dev', 'watch']);
};
