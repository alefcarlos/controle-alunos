(function() {
    'use strict';

    angular
        .module('SchoolControlApp.route', ['ngRoute'])
        .config(ModuleConfig);

    ModuleConfig.$inject = ['$routeProvider'];
    function ModuleConfig($routeProvider) {
        // $httpProvider.interceptors.push('InterceptorMain');

        $routeProvider
            .when('/', { redirectTo:'/home'})
            .when('/error',
                  {
                    templateUrl : 'views/errors/error.html',
                  })
            .otherwise({redirectTo:'/'});
    }
})();
