(function() {
    'use strict';

    angular
        .module('SchoolControlApp.factories', [])
        .factory('MaterialFunctions', MaterialFunctions)
        .factory('AppCommom', AppCommom);

    function MaterialFunctions() {
         return {
            makeSnackbar: function(msg){
                jq.snackbar({content: msg});
            },
            openModal: function(id){
                var query = '#' + id;
                jq(query).modal('show');
            },
            hideModal: function(id){
                var query = '#' + id;
                jq(query).modal('hide');
            },
            initTooltip: function () {
              jq('[data-toggle="tooltip"]').tooltip();
            }
        };
    }

    function AppCommom(appConfig) {
      return {
         isApiRequest: function(config){
           var url = config.url;

           //Validar somente response que sejam da api de controle alunos
           return (url.indexOf(appConfig.apiUrlBase) >= 0);
         },
         daysOfWeek: [
                      {id: 0, desc: 'dom'},
                      {id: 1, desc: 'seg'},
                      {id: 2, desc: 'ter'},
                      {id: 3, desc: 'qua'},
                      {id: 4, desc: 'qui'},
                      {id: 5, desc: 'sex'},
                      {id: 6, desc: 'sab'}
                    ],
          months: [
                       {id: 1, desc: 'jan'},
                       {id: 2, desc: 'fev'},
                       {id: 3, desc: 'mar'},
                       {id: 4, desc: 'abr'},
                       {id: 5, desc: 'mai'},
                       {id: 6, desc: 'jun'},
                       {id: 7, desc: 'jul'},
                       {id: 8, desc: 'ago'},
                       {id: 9, desc: 'set'},
                       {id: 10, desc: 'out'},
                       {id: 11, desc: 'nov'},
                       {id: 12, desc: 'dez'}
                     ]
     };
    }

    AppCommom.$inject = ['appConfig'];
})();
