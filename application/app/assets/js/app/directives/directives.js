(function() {
    'use strict';

    angular
        .module('SchoolControlApp.directives', [])
          .directive('materialIcon', MaterialIcon)
          .directive('materialIconModel', MaterialIconModel)
          .directive('materialIconBack', function() { return _materialIconCustom('arrow_back'); })
          .directive('materialIconMoney', function() { return _materialIconCustom('attach_money'); })
          .directive('materialIconEdit', function() { return _materialIconCustom('edit'); })
          .directive('materialIconDelete', function() { return _materialIconCustom('delete_forever'); })
          .directive('materialIconClass', function() { return _materialIconCustom('class'); })
          .directive('materialIconPlus', function() { return _materialIconCustom('add'); })
          .directive('materialIconPayment', function() { return _materialIconCustom('payment'); })
          .directive('materialIconMinus', function() { return _materialIconCustom('remove'); })
          .directive('materialIconSave', function() { return _materialIconCustom('save'); });

    function _materialIconCustom(icon) {
      return {
           restrict: 'AE',
           transclude: true,
           template: '<i class="material-icons">' + icon + '</i>'
         };
    }

    function MaterialIconModel() {
         return {
              restrict: 'AE',
              transclude: true,
              scope: {
                icon: '='
              },
              template: '<i class="material-icons">{{icon}}</i>'
            };
    }

    function MaterialIcon() {
         return {
              restrict: 'AE',
              transclude: true,
              scope: {
                icon: '@'
              },
              template: '<i class="material-icons">{{icon}}</i>'
            };
    }
})();
