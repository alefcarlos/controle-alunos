(function() {
    'use strict';

    angular
        .module('SchoolControlApp.course.route', ['ngRoute'])
        .config(ModuleConfig);

    ModuleConfig.$inject = ['$routeProvider'];
    function ModuleConfig($routeProvider) {
        var _controllerAs = 'Course';

        $routeProvider
            .when('/courses',
                  {
                    templateUrl : 'views/course/list.html',
                    controller: 'CourseController',
                    controllerAs: _controllerAs,
                    resolve: {
                      coursesList: function(CourseServiceAPI){
                        return CourseServiceAPI.getList();
                      }
                    }
                  })
            .when('/course/:id',
                  {
                    templateUrl : 'views/course/view.html',
                    controller: 'CourseViewController',
                    controllerAs: _controllerAs,
                    resolve: {
                       course : function($route, CourseServiceAPI){
                         return CourseServiceAPI.getById($route.current.params.id);
                       }
                     }
                  })
            .when('/course/edit/:id',
                  {
                    templateUrl : 'views/course/edit.html',
                    controller: 'CourseEditController',
                    controllerAs: _controllerAs,
                    resolve: {
                       course : function($route, CourseServiceAPI){
                         return CourseServiceAPI.getById($route.current.params.id);
                       }
                     }
                  })
            .when('/courses/new',
                  {
                    templateUrl : 'views/course/new.html',
                    controller: 'CourseNewController',
                    controllerAs: _controllerAs,
                  });
    }
})();
