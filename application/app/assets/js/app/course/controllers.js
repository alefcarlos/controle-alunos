(function() {
    'use strict';
    angular.module('SchoolControlApp.course.controllers', [])

    .controller('CourseController', CourseController)
    .controller('CourseViewController', CourseViewController)
    .controller('CourseEditController', CourseEditController)
    .controller('CourseNewController', CourseNewController);

    function CourseController($window, coursesList,CourseServiceAPI, MaterialFunctions) {
        var vm = this;

        vm.coursesList = coursesList;

        vm.delete = function(id){
          var confirm = $window.confirm('Deseja realmente excluir ?');
          if (!confirm)
            return;

          CourseServiceAPI.delete(id).then(function(){
              CourseServiceAPI.getList().then(function (data) {
                  MaterialFunctions.makeSnackbar('Curso deletado com sucesso.');
                  vm.coursesList = data;
              });
          });
        };
    }

    function CourseViewController(course) {
        var vm = this;

        vm.course = angular.copy(course);
    }

    function CourseEditController(course,CourseServiceAPI, MaterialFunctions, $location) {
        var vm = this;

        vm.course = angular.copy(course);

        vm.update = function (course) {
          CourseServiceAPI.update(course).then(function () {
            MaterialFunctions.makeSnackbar('Curso atualizado com sucesso.');
            $location.path('courses');
          });
        };
    }

    function CourseNewController($location, CourseServiceAPI, MaterialFunctions) {
        var vm = this;

        vm.add = function (course) {
          CourseServiceAPI.add(course).then(function () {
            MaterialFunctions.makeSnackbar('Curso criado com sucesso.');
            $location.path('courses');
          });
        };
    }

    CourseViewController.$inject  = ['course'];
    CourseEditController.$inject  = ['course', 'CourseServiceAPI', 'MaterialFunctions','$location'];
    CourseController.$inject  = ['$window', 'coursesList','CourseServiceAPI','MaterialFunctions'];
    CourseNewController.$inject = ['$location','CourseServiceAPI','MaterialFunctions'];
})();
