(function() {
    'use strict';

    angular
        .module('SchoolControlApp.course', [
                                            'SchoolControlApp.course.route',
                                            'SchoolControlApp.course.controllers',
                                            'SchoolControlApp.course.services',
                                            'SchoolControlApp.course.directives'
                                          ]);
})();
