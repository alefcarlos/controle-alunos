(function() {
    'use strict';
    angular.module('SchoolControlApp.course.services', [])

    .service('CourseServiceAPI', CourseServiceAPI);

    function CourseServiceAPI($http, appConfig) {
      var apiRoute = 'courses';
      var apiURL = appConfig.apiUrlBase + '/' + apiRoute;

      var _getById = function (id) {
        return $http.get(apiURL + '/' + id);
      };

      this.getList = function() {
        return $http.get(apiURL);
      };

      this.getById = function (id) {
        return _getById(id);
      };

      this.add = function(entity){
          var item = angular.copy(entity);
          return $http.post(apiURL, item);
      };

      this.delete = function (id) {
        return $http.delete(apiURL + '/' + id);
      };

      this.update = function (entity) {
        var item = angular.copy(entity);
        return $http.put(apiURL, item);
      };
    }

    CourseServiceAPI.$inject= ['$http', 'appConfig'];
})();
