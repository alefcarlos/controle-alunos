(function() {
    'use strict';
    angular.module('SchoolControlApp.payment.directives', [])
      .directive('addPayment', AddPayment);

    function AddPayment() {
      return {
           restrict: 'AE',
           transclude: true,
           templateUrl: 'views/payment/add-payment.html'
         };
    }
})();
