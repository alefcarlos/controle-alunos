(function() {
    'use strict';
    angular.module('SchoolControlApp.payment.services', [])

      .service('PaymentServiceAPI', PaymentServiceAPI);

    function PaymentServiceAPI($http,appConfig) {
      var apiRoute = 'payments';
      var apiURL = appConfig.apiUrlBase + '/' + apiRoute;

      this.export = function () {
        return $http.get(apiURL + '/export', {responseType:'arraybuffer'});
      };
    }

    PaymentServiceAPI.$inject  = ['$http','appConfig'];
})();
