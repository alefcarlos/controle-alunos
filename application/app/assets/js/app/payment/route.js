(function() {
    'use strict';

    angular
        .module('SchoolControlApp.payment.route', ['ngRoute'])
        .config(ModuleConfig);

    ModuleConfig.$inject = ['$routeProvider'];
    function ModuleConfig($routeProvider) {
        var _controllerAs = 'Pay';

        $routeProvider
            .when('/payment/register',
                  {
                    templateUrl : 'views/payment/register.html',
                    controller: 'PaymentRegisterController',
                    controllerAs: _controllerAs,
                    resolve: {
                      students: function (StudentServiceAPI) {
                         return StudentServiceAPI.getList();
                      }
                    }
                  });
    }
})();
