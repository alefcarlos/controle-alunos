(function() {
    'use strict';
    angular.module('SchoolControlApp.payment.controllers', [])

    .controller('PaymentRegisterController', PaymentRegisterController);

    function PaymentRegisterController(students, MaterialFunctions,
                                        StudentServiceAPI, AppCommom, FileSaver, Blob,
                                        PaymentServiceAPI) {
        var vm = this;

        vm.students = students;

        vm.showAddPayment = function (studentId) {
          vm.months = AppCommom.months;
          vm.payment = {
            paymentValue: 0,
            selectedMonth: vm.months[0]
          };

          StudentServiceAPI.getById(studentId).then(function (data) {
            vm.currentStudent = data;
            StudentServiceAPI.getValidCourses(studentId).then(function (data) {
              if (data.length)
              {
                vm.validCourses = data;
                MaterialFunctions.openModal("modalAddPayment");
              }
              else {
                MaterialFunctions.makeSnackbar('Aluno não tem cursos válidos para registrar pagamento.');
              }
            });
          });
        };

        vm.addPayment = function (studentId, payment) {
          var entity = {
            PaymentValue: payment.paymentValue,
            StudentId: studentId,
            CourseId: payment.selectedCourse.CourseId,
            Month: payment.selectedMonth.id
          };

          StudentServiceAPI.addPayment(studentId, entity).then(function () {
            MaterialFunctions.hideModal('modalAddPayment');
            MaterialFunctions.makeSnackbar('Pagamento registrado com sucesso.');
          });
        };

        vm.loadPayments =function (studentId) {
          StudentServiceAPI.getPayments(studentId).then(function (data) {
            if (data && data.length){
              vm.payments = data;
              MaterialFunctions.openModal('modalStudentPayments');
            }
            else
              MaterialFunctions.makeSnackbar('Estudante não tem mensalidade paga.');
          });
        };

        vm.export = function(text) {
          PaymentServiceAPI.export().then(function (data) {
              var d = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
              FileSaver.saveAs(d, 'pagamentos.xlsx');
          });
        };
    }

    PaymentRegisterController.$inject = ['students','MaterialFunctions','StudentServiceAPI', 'AppCommom',
                                        'FileSaver', 'Blob','PaymentServiceAPI'];
})();
