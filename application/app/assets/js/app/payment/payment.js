(function() {
    'use strict';

    angular
        .module('SchoolControlApp.payment', ['SchoolControlApp.payment.route',
                                              'SchoolControlApp.payment.controllers',
                                              'SchoolControlApp.payment.services',
                                              'SchoolControlApp.payment.directives',
                                              'ui.utils.masks'
                                            ]);
})();
