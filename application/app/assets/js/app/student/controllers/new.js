(function () {
  'use strict';
  angular.module('SchoolControlApp.student.controllers')

    .controller('StudentNewController', StudentNewController);

  function StudentNewController($location, StudentServiceAPI, MaterialFunctions, StudentCommom) {
    var vm = this;

    vm.genderList = StudentCommom.genderList;

    vm.add = function (student) {
      console.log(student);
      StudentServiceAPI.add(student).then(function () {
        MaterialFunctions.makeSnackbar('Estudante criado com sucesso.');
        $location.path('students');
      });
    };
  }

  StudentNewController.$inject = ['$location', 'StudentServiceAPI', 'MaterialFunctions', 'StudentCommom'];
})();
