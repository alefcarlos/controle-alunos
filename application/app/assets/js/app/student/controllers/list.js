(function () {
  'use strict';
  angular.module('SchoolControlApp.student.controllers')

    .controller('StudentController', StudentController);

  function StudentController($window, studentsList, StudentServiceAPI, MaterialFunctions,
                            FileSaver, Blob) {
    var vm = this;

    vm.studentsList = studentsList;
    vm.payments = {};

    //parâmetros de paginação (utilizado em requisição ajax)
    //vm.currentPage = 1;
    //vm.totalItems = studentsList.length;
    vm.itemsPerPage = 7;

    vm.export = function () {
      StudentServiceAPI.export().then(function (data) {
          var d = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
          FileSaver.saveAs(d, 'alunos.xlsx');
      });
    };

    vm.delete = function (id) {
      var confirm = $window.confirm('Deseja realmente excluir ?');
      if (!confirm)
        return;

      StudentServiceAPI.delete(id).then(function (params) {
        MaterialFunctions.makeSnackbar('Estudante deletado com sucesso.');
        StudentServiceAPI.getList().then(function (data) {
          vm.studentsList = data;
        });
      });
    };

    vm.loadCourses = function (id) {
      var student = StudentServiceAPI.getValidCourses(id).then(function (data) {
        vm.currentCourses = data;

        if (vm.currentCourses && vm.currentCourses.length)
          MaterialFunctions.openModal('modalStudentCourses');
        else
          MaterialFunctions.makeSnackbar('Estudante não tem cursos válidos.');
      });
    };

    vm.loadPayments = function (studentId) {
      StudentServiceAPI.getPayments(studentId).then(function (data) {
        if (data && data.length){
          vm.payments = data;
          MaterialFunctions.openModal('modalStudentPayments');
        }
        else
          MaterialFunctions.makeSnackbar('Estudante não tem mensalidade paga.');
      });
    };
  }

  StudentController.$inject = ['$window','studentsList', 'StudentServiceAPI', 'MaterialFunctions', 'FileSaver', 'Blob'];
})();
