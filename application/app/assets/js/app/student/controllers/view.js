(function () {
    'use strict';
    angular.module('SchoolControlApp.student.controllers')

        .controller('StudentViewController', StudentViewController);

    function StudentViewController($window, student, courses, MaterialFunctions, StudentServiceAPI, StudentCommom, AppCommom) {
        var vm = this;

        vm.student = student;
        vm.courses = courses;
        vm.payments = [];
        vm.genderList = StudentCommom.genderList;
        vm.student.genderDesc = StudentCommom.genderList[student.Gender].name;

        vm.isEnable = function (status) {
            return status ? 'remove' : 'add';
        };

        vm.loadPayments = function (studentId) {
          StudentServiceAPI.getPayments(studentId).then(function (data) {
            if (data && data.length){
              vm.payments = data;
              MaterialFunctions.openModal('modalStudentPayments');
            }
            else
              MaterialFunctions.makeSnackbar('Estudante não tem mensalidade paga.');
          });
        };

        vm.addCourse = function (studentId, course) {
            var _course = angular.copy(course.selectedCourse);
            _course.days = course.days;
            _course.Start = course.Start;

            if (!_course.days || !_course.days.length){
              MaterialFunctions.makeSnackbar('Selecione pelo menos um dia.');
              return;
            }

            StudentServiceAPI.addCourse(studentId, _course)
                .then(function (data) {
                    vm.courses = data;

                    MaterialFunctions.hideModal('modalStudentAddCourse');
                    MaterialFunctions.makeSnackbar('Curso adicionado com sucesso.');
                });
        };

        vm.showAddCourse = function (studentId) {
            vm.course = {
              Start: new Date().toISOString()
            };

            //Obter somente os cursos não associados
            StudentServiceAPI.getAvailableCourses(studentId)
                .then(function (data) {
                    if (data) {
                        if (data.length) {
                            vm.availableCourses = data;
                            vm.listOfDays = AppCommom.daysOfWeek;
                            // vm.listOfDays = ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab'];

                            MaterialFunctions.openModal('modalStudentAddCourse');
                        }
                        else {
                            MaterialFunctions.makeSnackbar('Todos os cursos já estão associados ao estudante!');
                        }
                    }
                });
        };

        vm.changeCourseState = function (studentId, courseId, currentState) {
          var msg = currentState ? 'Deseja desmatricular o aluno nesse curso ?' : 'Deseja rematricular o aluno nesse curso?';
          var confirm = $window.confirm(msg);
          if (!confirm) return;

          StudentServiceAPI.changeCourseState(studentId, courseId).then(function (data) {
              vm.courses = data;
              MaterialFunctions.makeSnackbar('Status do curso alterado com sucesso.');
          });
        };

    }

    StudentViewController.$inject = ['$window', 'student', 'courses', 'MaterialFunctions', 'StudentServiceAPI', 'StudentCommom','AppCommom'];
})();
