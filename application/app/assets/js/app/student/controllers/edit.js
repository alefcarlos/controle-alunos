(function () {
  'use strict';
  angular.module('SchoolControlApp.student.controllers')

    .controller('StudentEditController', StudentEditController);

  function StudentEditController(student, $location, StudentServiceAPI, MaterialFunctions, StudentCommom) {
    var vm = this;

    vm.student = student;
    vm.genderList = StudentCommom.genderList;

    // vm.student.Gender = vm.student.Gender;

    vm.update = function (student) {
      StudentServiceAPI.update(student).then(function () {
        MaterialFunctions.makeSnackbar('Estudante atualizado com sucesso.');
        $location.path('students');
      });
    };

  }
  StudentEditController.$inject = ['student', '$location', 'StudentServiceAPI', 'MaterialFunctions', 'StudentCommom'];
})();
