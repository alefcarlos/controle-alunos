(function() {
    'use strict';

    angular
        .module('SchoolControlApp.student.filters', [])
        .filter('gender', StudentGender);

    function StudentGender(StudentCommom) {
         return function (input){
           var result =
             StudentCommom.genderList.filter(function (e) {
               return e.id == input;
             });

           if (!result.length)
             return '';

           return result[0].desc;
        };
    }

    StudentGender.$inject = ['StudentCommom'];
})();
