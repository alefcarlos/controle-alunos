(function() {
    'use strict';
    angular.module('SchoolControlApp.student.services', [])

    .service('StudentServiceAPI', StudentServiceAPI);

    function StudentServiceAPI($http, appConfig) {
      var apiRoute = 'students';
      var apiURL = appConfig.apiUrlBase + '/' + apiRoute;

      var consolidateOperationRoute = function (studentId, route){
        studentId = studentId || '';
        route = route || '';

        var result = apiURL;
        result = result.concat('/' + studentId);
        result = result.concat('/' + route);

        return result;
      };

      this.getList = function() {
        return $http.get(consolidateOperationRoute());
      };

      this.getById = function (id) {
        return $http.get(consolidateOperationRoute(id));
      };

      this.getPayments = function (studentId) {
        return $http.get(consolidateOperationRoute(studentId, 'payments'));
      };

      this.getCourses = function (studentId) {
        return $http.get(consolidateOperationRoute(studentId, 'courses'));
      };

      this.add = function(student){
          var item = angular.copy(student);
          return $http.post(apiURL, item);
      };

      this.delete = function (id) {
        return $http.delete(apiURL + '/' + id);
      };

      this.getAvailableCourses = function (studentId) {
        return $http.get(consolidateOperationRoute(studentId, 'courses/available'));
      };

      this.getValidCourses = function (studentId) {
        return $http.get(consolidateOperationRoute(studentId, 'courses/valid'));
      };

      this.addCourse = function (studentId, course) {
        var _courseToAdd= {
          CourseId: course.CourseId,
          Days : course.days,
          Start: course.Start
        };

        return $http.post(consolidateOperationRoute(studentId, 'courses/'), _courseToAdd);
      };

      this.changeCourseState = function (studentId, courseId) {
        return $http.put(consolidateOperationRoute(studentId, 'courses/' + courseId));
      };

      this.addPayment = function (studentId, payment) {
        return $http.post(consolidateOperationRoute(studentId, 'courses/' + payment.CourseId + '/payment'), payment);
      };

      this.update = function (student) {
        console.log(student);
        var item = angular.copy(student);
        return $http.put(apiURL, item);
      };

      this.export = function () {
        return $http.get(apiURL + '/export', {responseType:'arraybuffer'});
      };
    }

    StudentServiceAPI.$inject  = ['$http','appConfig'];
})();
