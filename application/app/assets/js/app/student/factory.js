(function() {
    'use strict';

    angular
        .module('SchoolControlApp.student.factories', [])
        .factory('StudentCommom', StudentCommom);

    function StudentCommom() {
         return {
              genderList: [
                {id: 0, desc: 'Masculino'},
                {id:1,desc:'Feminino'}
              ]
            };
    }
})();
