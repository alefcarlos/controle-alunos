(function() {
    'use strict';

    angular
        .module('SchoolControlApp.student.route', ['ngRoute'])
        .config(ModuleConfig);

    ModuleConfig.$inject = ['$routeProvider'];
    function ModuleConfig($routeProvider) {
        var _controllerAs = 'Stud';

        $routeProvider
            .when('/students',
                  {
                    templateUrl : 'views/student/list.html',
                    controller: 'StudentController',
                    controllerAs: _controllerAs,
                    resolve: {
                      studentsList: function(StudentServiceAPI){
                        var list = StudentServiceAPI.getList();
                        return list;
                      }
                    }
                  })
            .when('/student/:id',
                  {
                    templateUrl : 'views/student/view.html',
                    controller: 'StudentViewController',
                    controllerAs: _controllerAs,
                    resolve: {
                      student : function($route, StudentServiceAPI){
                         return StudentServiceAPI.getById($route.current.params.id);
                      },
                      courses : function($route, StudentServiceAPI){
                         return StudentServiceAPI.getCourses($route.current.params.id);
                       }
                     }
                  })
            .when('/student/edit/:id',
                  {
                    templateUrl : 'views/student/edit.html',
                    controller: 'StudentEditController',
                    controllerAs: _controllerAs,
                    resolve: {
                       student : function($route, StudentServiceAPI){
                        return StudentServiceAPI.getById($route.current.params.id);
                       }
                     }
                  })
            .when('/students/new',
                  {
                    templateUrl : 'views/student/new.html',
                    controller: 'StudentNewController',
                    controllerAs: _controllerAs,
                  });
    }
})();
