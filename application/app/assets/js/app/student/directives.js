(function() {
    'use strict';
    angular.module('SchoolControlApp.student.directives', [])

    .directive('studentCoursesList', StudentCoursesList)
    .directive('studentCoursesListModel', StudentCoursesListModel)
    .directive('studentAssociateCourse', StudentAssociateCourse)
    .directive('studentPaymentsModel', StudentPaymentsModel)
    .directive('studentCoursesArea', StudentCoursesArea);

    function StudentCoursesList() {
      return {
           restrict: 'AE',
           transclude: true,
           templateUrl: 'views/student/courses-list.html'
         };
    }

    function StudentPaymentsModel() {
      return {
           restrict: 'AE',
           transclude: true,
           scope: {
             payments: '='
           },
           templateUrl: 'views/partials/payments.html'
         };
    }

    function StudentCoursesListModel() {
      return {
           restrict: 'AE',
           transclude: true,
           scope: {
             courses: '='
           },
           templateUrl: 'views/partials/student-courses-list.html'
         };
    }

    function StudentCoursesArea() {
      return {
           restrict: 'AE',
           transclude: true,
           templateUrl: 'views/student/courses.html'
         };
    }

    function StudentAssociateCourse() {
      return {
           restrict: 'AE',
           transclude: true,
           templateUrl: 'views/student/associate-course.html'
         };
    }
})();
