(function() {
    'use strict';

    angular
        .module('SchoolControlApp.student', ['SchoolControlApp.student.route',
                                              'SchoolControlApp.student.controllers',
                                              'SchoolControlApp.student.services',
                                              'SchoolControlApp.student.factories',
                                              'SchoolControlApp.student.directives',
                                              'SchoolControlApp.student.filters',
                                              'checklist-model',
                                              'ui.utils.masks',
                                              'ui.bootstrap.datetimepicker'
                                            ]);
})();
