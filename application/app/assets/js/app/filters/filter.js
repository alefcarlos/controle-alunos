(function() {
    'use strict';

    angular
        .module('SchoolControlApp.filters', [])
        .filter('dayName', DayName)
        .filter('monthOfYear', MonthOfYear);

        function DayName(AppCommom) {
          return function (input) {
            var result =
              AppCommom.daysOfWeek.filter(function (e) {
                return e.id == input;
              });

            if (!result.length)
              return '';

            return result[0].desc;
          };
        }

        function MonthOfYear(AppCommom) {
          return function (input) {
            var result =
              AppCommom.months.filter(function (e) {
                return e.id == input;
              });

            if (!result.length)
              return '';

            return result[0].desc;
          };
        }

        DayName.$inject = ['AppCommom'];
        MonthOfYear.$inject = ['AppCommom'];
})();
