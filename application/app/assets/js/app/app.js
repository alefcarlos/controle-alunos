(function() {
    'use strict';

    angular
        .module('SchoolControlApp', ['SchoolControlApp.home',
                                      'SchoolControlApp.student',
                                      'SchoolControlApp.course',
                                      'SchoolControlApp.payment',
                                      'SchoolControlApp.factories',
                                      'SchoolControlApp.directives',
                                      'SchoolControlApp.constants',
                                      'SchoolControlApp.interceptors',
                                      'SchoolControlApp.route',
                                      'SchoolControlApp.filters',
                                      'ngFileSaver',
                                      'angularUtils.directives.dirPagination'
                                    ]);

    angular.module('SchoolControlApp.student.controllers', []);
    angular.module('SchoolControlApp.interceptors', []);
})();
