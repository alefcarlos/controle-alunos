(function() {
    'use strict';

    angular
        .module('SchoolControlApp.interceptors')
        .factory('InterceptorLoad', InterceptorLoad);

        function InterceptorLoad($q, MaterialFunctions) {
             var _numLoadings = 0;

            return {

                request : function(config){
                    _numLoadings++;

                    // Show loader
                    MaterialFunctions.openModal('modalLoading');
                    //$rootScope.$broadcast("loader_show");
                    return config;
                },

                response : function(response){
                    if ((--_numLoadings) === 0) {
                        // Hide loader
                        MaterialFunctions.hideModal('modalLoading');
                        //$rootScope.$broadcast("loader_hide");
                    }

                    return response;
                },

                responseError: function (response) {

                    if (!(--_numLoadings)) {
                        // Hide loader
                        MaterialFunctions.hideModal('modalLoading');
                        //$rootScope.$broadcast("loader_hide");
                    }

                    return $q.reject(response);
                }
            };
        }

        InterceptorLoad.$inject = ['$q', 'MaterialFunctions'];
})();
