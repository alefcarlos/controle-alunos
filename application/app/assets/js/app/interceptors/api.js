(function() {
    'use strict';

    angular
        .module('SchoolControlApp.interceptors')
        .factory('InterceptorApi', InterceptorApi);

        function InterceptorApi($q, AppCommom, $location, MaterialFunctions) {

            var redirectToError = function (error) {
                $location.path('error');
            };

            return {
                response: function (response) {

                  // Validar se não estamos requisitanto um arquivo...
                  if (response.data instanceof ArrayBuffer)
                    return response.data;

                  if (!AppCommom.isApiRequest(response.config))
                      return response;

                  //Validar se houve algum erro.
                  var data = response.data;

                  if (!data.Status){
                      MaterialFunctions.makeSnackbar('Erro requisição API: ' + data.Message);
                      return $q.reject(response);
                  }

                  return response.data.Data;
                },
                responseError: function (rejection) {
                    if (!AppCommom.isApiRequest(rejection.config))
                        return $q.reject(rejection);

                    MaterialFunctions.makeSnackbar('Erro requisição API: ' + rejection.statusText);

                    return $q.reject(rejection);
                }
            };
        }

        InterceptorApi.$inject = ['$q', 'AppCommom', '$location','MaterialFunctions'];
})();
