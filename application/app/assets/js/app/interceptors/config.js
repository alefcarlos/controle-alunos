(function() {
    'use strict';

    angular
        .module('SchoolControlApp.interceptors')
        .config(InterceptorConfig);

        function InterceptorConfig($httpProvider) {
            $httpProvider.interceptors.push('InterceptorApi');
            $httpProvider.interceptors.push('InterceptorError');
            $httpProvider.interceptors.push('InterceptorLoad');
        }

        InterceptorConfig.$inject = ['$httpProvider'];
})();
