(function() {
    'use strict';

    angular
        .module('SchoolControlApp.interceptors')
        .factory('InterceptorError', InterceptorError);

        function InterceptorError($q, $location, AppCommom) {
            var redirectToError = function (error) {
                $location.path('error');
            };

            return {
                responseError: function (rejection) {
                    if (AppCommom.isApiRequest(rejection.config))
                      return $q.reject(rejection);

                    redirectToError();

                    return $q.reject(rejection);
                }
            };
        }

    InterceptorError.$inject = ['$q',  '$location', 'AppCommom'];
})();
