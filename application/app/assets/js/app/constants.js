(function() {
    'use strict';

    angular
        .module('SchoolControlApp.constants', [])
        .constant('appConfig', {
            'apiUrlBase': 'http://localhost/controle-alunos/api'
        });
})();