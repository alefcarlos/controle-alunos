(function() {
    'use strict';

    angular
        .module('SchoolControlApp.home', ['SchoolControlApp.home.route',
                                          'SchoolControlApp.home.controllers',
                                          'SchoolControlApp.student.services'
                                        ]);
})();
