(function() {
    'use strict';

    angular
        .module('SchoolControlApp.home.route', ['ngRoute'])
        .config(ModuleConfig);

    ModuleConfig.$inject = ['$routeProvider'];
    function ModuleConfig($routeProvider) {
        // $httpProvider.interceptors.push('InterceptorMain');

        $routeProvider
            .when('/home',
                  {
                    templateUrl : 'views/home/home.html',
                    controller: 'HomeController',
                    controllerAs: 'Home'
                  });
    }
})();
